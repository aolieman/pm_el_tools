# -*- coding: utf-8 -*-
"""
Created on Fri Jan 30 23:16:40 2015

@author: Alex
"""
from __future__ import division
import random, json
from collections import defaultdict
from member_index import members_per_department
from pm_el_tools.pm_search_client import pm_search, member_autocomplete
from pm_el_tools import helpers
from pm_el_tools.settings import DB
from pool_annotations import pool_for_proceedings

# ensure the sample collection is indexed
for collection in ('sample', 'annotated_sample', 'rendered_sample'):
    DB[collection].ensure_index(
        [("annotated_by" , 1), ("department", 1), ("topic_id", 1)],
        name='annotator_topic_and_dept' 
    )

def get_topics_per_department():
    fpath = '../models/intermediary/topics_per_department.json'
    try:
         with open(fpath, 'rb') as f:
             return json.load(f)
    except IOError:
        print "{} was not found; fetching from politicalmashup.nl".format(fpath)
             
    topics_per_dep = {}
    m_per_dep = members_per_department()
    for department, memberships in m_per_dep.iteritems():
        topics_per_dep[department] = set()
        for mship in memberships:
            print "Getting scenes for", department, mship
            scene_res = pm_search(
                speakers=mship[0],
                startdate=mship[2],
                enddate=mship[3],
                role='government',
                granularity='scene',
                order='chrono'
            )
            for res in scene_res:
                speech_id = res['xml url'].split('.nl/', 1)[1]
                topic_id = helpers.get_topic_id(speech_id)
                topics_per_dep[department].add(topic_id)
                
    with open(fpath, 'wb') as f:
        json.dump(topics_per_dep, f, indent=2, sort_keys=True, default=list)
                
    return topics_per_dep
    
def get_sample_of_scenes(char_quotum, quotum_function=len):
    # keep a record of topics that have been included in the sample    
    visited_topics = set(DB.sample.distinct('topic_id'))
    
    # get topic identifiers and (per-department) quota
    tpd = {k: set(v) for k, v in get_topics_per_department().iteritems()}
    quotum_state = {k: 0 for k in tpd}
    quotum_state['overall'] = 0
    
    if quotum_function:
        # character quota proportional to some indicator
        quota_indicator = {k: quotum_function(v) for k, v in tpd.iteritems()}
        indicator_total = sum(quota_indicator.values())
        relative_quota = {k: (v / indicator_total) for k, v in quota_indicator.iteritems()}
    else:
        relative_quota = {k: (v / len(tpd)) for k, v in tpd.iteritems()}
        
    per_department_quota = {k: int(v * char_quotum) for k, v in relative_quota.iteritems()}
    print "Per-department quota as a fraction of the overall character quotum:"
    sorted_quota = sorted(relative_quota.items(), key=lambda t: t[1], reverse=True)
    for t in sorted_quota:
        print "{:<50} {:.2f}".format(t[0], t[1])
    print "\n\n"
    
    # keep a record of short scenes per department
    short_scenes = {k: {'len': char_quotum, 'scene': None, 'tries': 0} for k in tpd}
    
    while quotum_state['overall'] < char_quotum:
        # for each department, pick a topic and scene
        for dept, topic_ids in tpd.iteritems():
            diff = list(topic_ids.difference(visited_topics))
            random.shuffle(diff)
            
            scene_is_added_to_sample = False
            while not scene_is_added_to_sample:
                # pick a topic that isn't represented in the sample
                try:
                    topic_id = diff.pop()
                    visited_topics.add(topic_id)
                except IndexError:
                    break
                print "Trying {}...".format(topic_id)
                
                # pick a scene that fits in the department quotum
                scene_list = helpers.scenes_for_topic(topic_id)
                random.shuffle(scene_list)
                while scene_list:
                    scene = scene_list.pop()
                    scene_len = len(str(scene.find_all(text=True)))
                    
                    if quotum_state[dept] + scene_len <= per_department_quota[dept]:
                        print "\nPicked for {} (len {}):".format(dept, scene_len)
                        print save_scene(scene, dept), '\n'
                        scene_is_added_to_sample = True
                        quotum_state[dept] += scene_len
                        quotum_state['overall'] += scene_len
                        break
                    else:
                        short_scenes[dept]['tries'] += 1
                        if scene_len < short_scenes[dept]['len']:
                            # scene length exceeds quotum, use as back-up
                            short_scenes[dept]['len'] = scene_len
                            short_scenes[dept]['scene'] = scene
                    
                    if (short_scenes[dept]['tries'] >= 10 and
                        short_scenes[dept]['len'] < 0.5 * per_department_quota[dept]):
                        # pick the shortest back-up scene with max. length 50% of quotum
                        print "\nPicked for {} (len {}):".format(dept, scene_len)
                        print save_scene(short_scenes[dept]['scene'], dept), '\n'
                        scene_is_added_to_sample = True
                        quotum_state[dept] += scene_len
                        quotum_state['overall'] += scene_len
                        tpd[dept] = set()
                        break
                        
    # return relative quotum state
    overall_state = quotum_state.pop('overall')
    relative_state = {k: (v / per_department_quota[k]) 
                      for k, v in quotum_state.iteritems()}
    relative_state['overall'] = overall_state / char_quotum

    print "\nQuotum completeness per department:"
    sorted_state = sorted(relative_state.items(), key=lambda t: t[1], reverse=True)
    for t in sorted_state:
        print "{:<50} {:.2f}".format(t[0], t[1])    
    return relative_state

def save_scene(scene_tag, department):
    mongo_doc = {
        '_id': scene_tag['pm:id'],
        'topic_id': helpers.get_topic_id(scene_tag['pm:id']),
        'annotated_by': [],
        'department': department,
        'xml': unicode(scene_tag)
    }
    return DB.sample.insert(mongo_doc)
    

def pool_annotations_for_sample():
    for scene in DB.sample.find():
        print "Pooling annotations for {}".format(scene['topic_id'])
        proc_xml = helpers.resolve_document(scene['topic_id'])
        ann_soup = pool_for_proceedings(proc_xml)
        fpath = "../annotated_output/xml/{}.xml".format(scene['topic_id'])
        with open(fpath, 'wb') as f:
            f.write(ann_soup.prettify('utf8'))
        scene_tag = ann_soup.find('scene', {'pm:id': scene['_id']})
        scene['xml'] = unicode(scene_tag)
        DB.annotated_sample.insert(scene)
        
def render_scene_html(sample_scene):
    scene_id = sample_scene['_id']
    scene_xml = helpers.BeautifulSoup(sample_scene['xml'], 'xml')
    scene_html, html_soup = helpers.get_scene_html(scene_id)
    
    # remove top-level stage directions
    for st_dir in scene_html.find_all('div', class_='stage-direction', recursive=False):
        st_dir.decompose()
    
    # remove reference-links, except those in (speech) headers
    for rl in scene_html.find_all('span', class_='reference-links'):
        if rl.parent.name[0] != 'h':
            rl.decompose()
    
    # group links by paragraph and surface form
    links_by_span = defaultdict(dict)
    for link in scene_xml.find_all('link'):
        p_tag = scene_html.find(id=link['entity-element'])
        if not p_tag: # annotation on stage-direction
            link.decompose() # remove annotation from xml
            continue
        
        if not link.text:
            if link.get('party-ref'):
                link.string = u"http://resolver.politicalmashup.nl/{}".format(link['party-ref'])
            elif link.get('member-ref'):
                if ',' in link['member-ref']:
                    print 'Bad annotation: multiple candidates', link
                    continue
                link.string = u"http://resolver.politicalmashup.nl/{}".format(link['member-ref'])
            else:
                print "Link without entity", link
                continue
        
        span = (int(link['entity-start']), int(link['entity-end']))
        new_surfaceform = True
        for existing_span in links_by_span[p_tag].keys():
            if helpers.annotation_encompasses(existing_span, span):
                links_by_span[p_tag][existing_span].add(link.text)
                new_surfaceform = False
            elif helpers.annotation_encompasses(span, existing_span):
                links_by_span[p_tag][span] = links_by_span[p_tag][existing_span] | {link.text}
                del links_by_span[p_tag][existing_span]
                new_surfaceform = False
            if not new_surfaceform:
                break
            
        if new_surfaceform:
            links_by_span[p_tag][span] = {link.text}
    
    # highlight links and add select tags
    annotation_ids = []
    for p_tag, span_links in links_by_span.iteritems():
        p_text = p_tag.text
        p_tag.clear()
        cursor = 0
        select_container = html_soup.new_tag('div')
        select_container['class'] = 'select-container'
        p_tag.insert_after(html_soup.new_string('\n'))
        p_tag.insert_after(select_container)
        
        for span, links in sorted(span_links.items(),
                                  key=lambda t: t[0][0]):
            p_tag.append(p_text[cursor:span[0]])
            new_span = html_soup.new_tag('span')
            new_span['data-select-ref'] = 'sys{}-{}_{}'.format(span[0], span[1], p_tag['id'])
            annotation_ids.append(new_span['data-select-ref'])
            new_span['class'] = 'system-hl'
            new_span.string = p_text[span[0]:span[1]]
            p_tag.append(new_span)
            cursor = span[1]
            
            # add links as select options
            new_select = html_soup.new_tag('select',
                                           style='display: none',
                                           multiple=None)
            new_select['class'] = "selectpicker sys{}-{}_{}".format(span[0], span[1], p_tag['id'])
            new_select['data-selected-text-format'] = "count"
            new_select['data-width'] = "37%"
            select_container.append(new_select)
            
            # add a removal prompt button
            remove_option = html_soup.new_tag('button', type='button')
            remove_option['class'] = "btn btn-default"
            remove_option['data-toggle'] = "modal"
            remove_option['data-target'] = "#remove-prompt"
            remove_option['data-annid'] = new_span['data-select-ref']
            remove_icon = html_soup.new_tag('span')
            remove_icon['class'] = "glyphicon glyphicon-remove"
            remove_option.append(remove_icon)
            new_select.insert_after(remove_option)
            
            existing_entities = links.copy()
            for link_url in links:
                new_option = html_soup.new_tag('option', value=link_url)
                # TODO: use linktype "politicalmashup"
                if 'resolver' in link_url:
                    slug = link_url.split('.nl/')[-1]
                    info_url = link_url + '.html'
                else:
                    slug = helpers.slug_from_dbp_url(link_url)
                    info_url = "http://nl.wikipedia.org/wiki/{}".format(slug)
                    new_option['value'] = info_url
                    existing_entities.add(info_url)
                new_option['data-content'] = "<span onclick='openBlank(&#39;{}&#39;)'><i class='glyphicon glyphicon-new-window'></i></span> {}".format(info_url, slug.replace('_', ' '))
                new_option.string = slug.replace('_', ' ')
                new_select.append(new_option)
                
            # add pm and wiki options
            for link_url, title in helpers.wikipedia_search(new_span.text):
                if link_url in existing_entities:
                    # TODO: why are there duplicate urls in the output? quoted urls
                    continue
                new_option = html_soup.new_tag('option', value=link_url)
                new_option['data-content'] = "<span onclick='openBlank(&#39;{}&#39;)'><i class='glyphicon glyphicon-new-window'></i></span> {}".format(link_url, title)
                new_option.string = title
                new_select.append(new_option)
                
            for m in member_autocomplete(new_span.text)[:5]:
                link_url = u"http://resolver.politicalmashup.nl/{}".format(m['id'])
                if link_url in existing_entities:
                    continue
                new_option = html_soup.new_tag('option', value=link_url)
                new_option['data-content'] = "<span onclick='openBlank(&#39;{}&#39;)'><i class='glyphicon glyphicon-new-window'></i></span> {}".format(link_url, m['label'])
                new_option.string = m['label']
                new_select.append(new_option)
                                               
        # add the final part of the paragraph text
        p_tag.append(p_text[cursor:])
        
    # add a button to check if the scene is done
    done_button = html_soup.new_tag('button', type='button')
    done_button['class'] = "btn btn-default"
    done_button['id'] = 'done-button'
    done_button['style'] = 'margin: 40px;'
    done_button.string = 'Done'
    done_icon = html_soup.new_tag('span')
    done_icon['class'] = "glyphicon glyphicon-ok"
    done_button.append(done_icon)
    scene_html.append(done_button)
        
    sample_scene['xml'] = scene_xml
    sample_scene['html'] = u'\n'.join([unicode(t) for t in html_soup.body.children])
    sample_scene['annotation_ids'] = annotation_ids
    return sample_scene
    
   
def render_html_for_sample():
    for scene in DB.annotated_sample.find():
        print scene['department'], scene['_id']
        scene = render_scene_html(scene)
        scene['xml'] = unicode(scene['xml'])
        scene['html'] = unicode(scene['html'])
        DB.rendered_sample.save(scene)
        