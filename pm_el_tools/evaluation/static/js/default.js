﻿// TODO: use yui, make text highlighter
var highlight_hash = function() {
  try {
    var hash = window.location.hash;
    var highlightable_p = document.getElementById(hash.substr(1));
    highlightable_p.className = highlightable_p.className + ' selected-pm';
  }
  catch (e) {}
};


var highlightAnn = function(status, annId) {
    if (status == 'annotate') {
        $('*[data-select-ref="'+annId+'"]').removeClass().addClass('annotated-hl');
    } else if (status == 'reset') {
        $('*[data-select-ref="'+annId+'"]').removeClass().addClass('system-hl');
    } else if (status == 'remove') {
        $('*[data-select-ref="'+annId+'"]').removeClass().addClass('removed-hl');
    } else if (status == 'not-done') {
        $('*[data-select-ref="'+annId+'"]').removeClass().addClass('not-done-hl');
    } else if (status == 'agreement') {
        $('*[data-select-ref="'+annId+'"]').removeClass().addClass('agreed-hl');
    }
}

$(function() {
   var actionUrl = "http://u027722.hum.uva.nl:8083/action/"+annotator.handle;
   
   var putAction = function(jsonAction, callback) {
        $.ajax({
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            url: actionUrl,
            data: JSON.stringify(jsonAction),
            dataType: "json",
            success: function (msg) {
                console.log(msg);
                if (msg.action === 'annotate' && !(jsonAction.url.length)){
                    highlightAnn('reset', msg.object_id);
                } else {
                    highlightAnn(msg.action, msg.object_id);
                }
                callback(msg);
            },
            error: function (err){
                console.log("Error:");
                console.log(err);
            }
        });
    }

  // bind selection changes to PUT
    $('.selectpicker').on('change', function(){
    //array with selected items
        var selected = $(this).find("option:selected");
        var arr = jQuery.makeArray(selected);
        var ann_id = $(this).attr("class").replace("selectpicker ","");
        var urlArr = [];
        $( arr ).each(function( index ) {
            temp = $( this ).val();
            urlArr.push(temp);
        });
        var jsonAction = {"action": "annotate", "object_id": ann_id, "url": urlArr };
        putAction(jsonAction, function(){ });
    });

    // bind remove buttons to the modal
    $('#remove-prompt').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var ann_id = button.data('annid');
      $('#remv_ann_id').val(ann_id);
      var modal = $(this);
      modal.find('.modal-title').text('Remove or modifiy annotation ' + ann_id);
    })

    // bind submit-remove to PUT
    $('#submit-remove-prompt').on('click', function (event) {
        var ann_id_remv = $('#remv_ann_id').val();
        var manual_url = $('#manual-url').val();
        var jsonAction
        if (manual_url) {
            jsonAction = {"action": "annotate", "object_id": ann_id_remv, "url": manual_url.split(' ') };
            $('#manual-url').val('');
        } else {
            jsonAction = {"action": "remove", "object_id": ann_id_remv };
        }
        putAction(jsonAction, function(){ 
            $('#remove-prompt').modal('hide');
            var annClassSelect = ann_id_remv.replace(/\./g, '\\.');
            $('.bootstrap-select.'+annClassSelect).find('span.filter-option.pull-left').text(manual_url);
        })
    })
    
    // Highlight which annotations are done and which aren't
    var highlightDone = function(alertBool) {
        var splitPath = window.location.pathname.split('/')
        var validationAction = {"action": "done", "object_id": splitPath.pop() };
        putAction(validationAction, function(msg){
            // iterate over msg.annotated and msg.removed, highlight each annotation and set dropdown text
            $.each(msg.annotated, function( idx, val ) { 
                highlightAnn('annotate', val);
                $('.bootstrap-select.'+val.replace(/\./g, '\\.')).find('span.filter-option.pull-left').text('*annotated*');
            });
            $.each(msg.removed, function( idx, val ) { 
                highlightAnn('remove', val);
                $('.bootstrap-select.'+val.replace(/\./g, '\\.')).find('span.filter-option.pull-left').text('*removed*');
            });
            
            // highlight already agreed-upon annotations and disable their select dropdowns and removal buttons
            $.each(msg.agreed, function( idx, val ) { 
                highlightAnn('agreement', val);
                $('.selectpicker.'+val.replace(/\./g, '\\.')).prop('disabled',true);
                $('.selectpicker.'+val.replace(/\./g, '\\.')).selectpicker('refresh');
                $('button[data-annid="'+val+'"]').addClass('disabled');
            });
            
            if (alertBool === true) {
                $.each(msg.not_done, function( idx, val ) { 
                    highlightAnn('not-done', val);
                });
            }
            
            // alert the user of incomplete annotations, go to overview if done
            if (msg.not_done.length && alertBool === true) {
                alert(msg.not_done.length + ' annotation(s) to do for this scene');
                ohash = window.location.hash;
                window.location.hash = '';
                window.location.hash = ohash;
            } else if (alertBool === true) {
                window.location.hash = '';
                window.location.pathname = '/' + splitPath.pop() + '/overview';
            }
        });
    }
    
    // highlight on page load without alert
    highlightDone(false);
    
    // bind highlight with alert to done button
    $('#done-button').on('click', function (event) {
        highlightDone(true);
    });
    
});