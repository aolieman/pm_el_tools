# -*- coding: utf-8 -*-
"""
Created on Fri Feb 06 18:26:05 2015

@author: Alex
"""
import json
from flask import Flask, render_template, url_for, abort, redirect, request, jsonify
from datetime import datetime
from pm_el_tools.settings import DB, INTERFACE_HOST, INTERFACE_PORT, INTERFACE_DEBUG

app = Flask(__name__)

@app.route('/<handle>/overview')
def overview(handle):
    get_annotator(handle)
    sample_coll = get_collection_name(handle)
    all_scenes = DB[sample_coll].find().sort('_id', 1)
    next_todo = None
    usr_progress = []
    for scene in all_scenes:
        if not next_todo and not handle in scene['annotated_by']:
            next_todo = url_for(
                'annotate_scene', handle=handle, topic_id=scene['topic_id'], _external=True
            ) + '#{}'.format(scene['_id'])
            
        scene_stats = {
            'url': url_for(
                'annotate_scene', handle=handle, topic_id=scene['topic_id']
            ) + '#{}'.format(scene['_id']),
            'label': scene['_id'],
            'department': scene['department'],
            'done': handle in scene['annotated_by']
        }
        usr_progress.append(scene_stats)
    
    return render_template('overview.html',
                           handle=handle,
                           next_scene=next_todo,
                           usr_progress=usr_progress)
                           
@app.route('/<handle>/static/<path:fp>')
def redirect_static(handle, fp):
    return redirect(url_for('static', filename=fp))
    
@app.route('/<handle>/<topic_id>')
def annotate_scene(handle, topic_id):
    annotator = get_annotator(handle)
    del annotator['_id']
    json_annotator = json.dumps(annotator)
    sample_coll = get_collection_name(handle)
    topic = DB[sample_coll].find_one({'topic_id': topic_id})
    return render_template('topic.html', topic_body=topic['html'], annotator=json_annotator)
    
    
def get_annotator(handle):
    annotator = DB.annotator.find_one({'handle': handle})
    if annotator:
        return annotator
    else:
        abort(404)
        
@app.route('/action/<handle>', methods=['POST', 'PUT'])
def annotator_action(handle):
    """Update an annotation or progress in the history (and sample)
    """
    payload = request.get_json()
    action = payload['action']
    object_id = payload['object_id']
    
    sample_coll = get_collection_name(handle)
    
    action_doc = {
        '_id': '{}_{}'.format(handle, object_id),
        'handle': handle,
        'action': action,
        'object_id': object_id,
        'at_time': datetime.now()
    }
    
    if action == 'done':
        not_done, annotated, removed, agreed = get_annotation_status(object_id, handle)
        if len(not_done) == 0:
            # record that <handle> is done with this scene
            action_doc['done'] = True
            DB.history.save(action_doc)
            # mark this scene as annotated by <handle>
            topic = DB[sample_coll].find_one({'topic_id': object_id})
            topic['annotated_by'].append(handle)
            DB[sample_coll].save(topic)
        return jsonify(action=action, object_id=object_id,
                       not_done=not_done, annotated=annotated, 
                       removed=removed, agreed=agreed)
                       
    elif action in ('annotate', 'remove'):
        # record <handle>'s annotation
        if action == 'remove':
            # record that this annotation is /NIL/ according to <handle>
            target_entity = ['NIL']
        else:
            target_entity = payload['url']
        
        try:
            span_str = object_id.split('_')[0][3:]
            action_doc['link'] = {
                'url': target_entity,
                'start': int(span_str.split('-')[0]),
                'end': int(span_str.split('-')[1]),
                'element_id': object_id.split('_')[1],
                'annotator': handle
            }
            DB.history.save(action_doc)
        except ValueError:
            # malformed object_id (could be prefixed with handle)
            abort(400)
        
    else:
        # malformed query: unrecognized action
        abort(400)
    
    return jsonify(action=action, object_id=object_id)
    

def get_annotation_status(topic_id, handle):
    sample_coll = get_collection_name(handle)
    topic = DB[sample_coll].find_one({'topic_id': topic_id})
    not_annotated = []
    annotated = []
    removed = []
    agreed = []
    to_annotate = topic['annotation_ids']
    for ann_id in to_annotate:
        cursor = DB.history.find({'_id': '{}_{}'.format(handle, ann_id)}).limit(1)
        try:
            annotation = cursor.next()
            if annotation['action'] == 'annotate':
                if annotation['link']['url']:
                    annotated.append(ann_id)
                else:
                    not_annotated.append(ann_id)
            elif annotation['action'] == 'remove':
                removed.append(ann_id)
            elif annotation['action'] == 'agreement':
                agreed.append(ann_id)
            else:
                raise ValueError("Action {} was not recognized".format(annotation['action']))
        except StopIteration:
            not_annotated.append(ann_id)
    return not_annotated, annotated, removed, agreed
    
    
def get_collection_name(handle):
    if handle.startswith('consensus'):
        return 'consensus_sample'
    else:
        return 'rendered_sample'

if __name__ == '__main__':
    app.run(host=INTERFACE_HOST, port=INTERFACE_PORT, debug=INTERFACE_DEBUG)
    pass