import re, csv
from collections import defaultdict
from datetime import datetime
from pm_el_tools import helpers
from pm_el_tools.settings import DB


def save_annotator_disagreement(handle_a, handle_b, verbosity=0):
    per_sf = defaultdict(lambda: defaultdict(list))
    # cluster annotations in the history collection (incl. NIL) per SF
    for action in DB.history.find({'link': {'$exists': 1}}):
        if action['handle'] in (handle_a, handle_b):
            scene_id = helpers.get_scene_id(action['link']['element_id'])
            per_sf[scene_id][action['object_id']].append(action['link'])
            
    # remove Minister, Staatssecretaris, Staatssecretaris_(Nederland)
    urls_to_remove = {
            'http://nl.wikipedia.org/wiki/Minister',
            'http://nl.wikipedia.org/wiki/Staatssecretaris',
            'http://nl.wikipedia.org/wiki/Staatssecretaris_(Nederland)'
    }
              
    # for each rendered scene
    for scene_id, sfid_urllists in per_sf.iteritems():
        scene = DB.rendered_sample.find_one(scene_id)
        scene['html'] = helpers.BeautifulSoup(scene['html'], 'html5lib')
        if verbosity:
            print "Checking agreement for {} ({} annotations)".format(scene_id, len(sfid_urllists))
        # for each surface form
        for sf_id, url_lists in sfid_urllists.iteritems():            
            # find disagreements between annotators
            assert len(url_lists) == 2
            if isinstance(url_lists[0]['url'], list):
                urls_0 = {helpers.force_unquote(u) for u in url_lists[0]['url'] 
                          if u not in urls_to_remove}
                urls_0 = urls_0 or {'NIL'}
            else:
                # TODO: remove when NILs are given in lists
                urls_0 = {url_lists[0]['url']}
            if isinstance(url_lists[1]['url'], list):
                urls_1 = {helpers.force_unquote(u) for u in url_lists[1]['url'] 
                          if u not in urls_to_remove}
                urls_1 = urls_1 or {'NIL'}
            else:
                urls_1 = {url_lists[1]['url']}
            disagreement = urls_0.symmetric_difference(urls_1)
                
            # TODO: make solution rules for structural disagreements configurable
            # ignore Voorzitter annotations (url='ignore')
            if 'http://nl.wikipedia.org/wiki/Voorzitter' in disagreement:
                disagreement = None
                urls_0 = {'ignore'}
                
            if verbosity > 1 and disagreement:
                print "  disagreement for {}".format(sf_id)
                for url in disagreement:
                    print "    {}".format(url)
            
            # find corresponding select tag
            select = scene['html'].find('select', class_=sf_id)      
            
            # diff: options = union(urls_0, urls_1)
            if disagreement:
                urls = set()
                for url in urls_0.union(urls_1):
                    for suffix in ('.html', '?view=html'):
                        if url.endswith(suffix):
                            url = url[:-len(suffix)]
                    urls.add(url)
                set_options(select, urls, scene['html'])
            # no diff: options = url_set, history.save(ann)
            else:
                set_options(select, urls_0, scene['html'])
                agreement_action('consensus', sf_id, urls_0)
                
        # save the scene into a (new) consensus_sample collection
        scene['html'] = unicode(scene['html'])
        DB.consensus_sample.save(scene)
        
def set_options(select_tag, urls, html_soup):
    select_tag.clear()
    for url in urls:
        new_option = html_soup.new_tag('option', value=url)
        # TODO: use linktype "politicalmashup"
        if 'resolver' in url:
            slug = url.split('.nl/')[-1]
            url = url + '.html'
        elif 'encyclo.nl' in url:
            slug = url.split('.nl/')[-1]
        else:
            slug = url.split('wiki/')[-1]
        new_option['data-content'] = "<span onclick='openBlank(&#39;{}&#39;)'><i class='glyphicon glyphicon-new-window'></i></span> {}".format(url, slug.replace('_', ' '))
        if url == 'NIL':
            new_option['data-content'] = " NIL"
        new_option.string = slug.replace('_', ' ')
        select_tag.append(new_option)
        
def agreement_action(handle, sf_id, urls):
    action_doc = {
        '_id': '{}_{}'.format(handle, sf_id),
        'handle': handle,
        'action': 'agreement',
        'object_id': sf_id,
        'at_time': datetime.now()
    }
    span_str = sf_id.split('_')[0][3:]
    action_doc['link'] = {
        'url': list(urls),
        'start': int(span_str.split('-')[0]),
        'end': int(span_str.split('-')[1]),
        'element_id': sf_id.split('_')[1],
        'annotator': handle
    }
    DB.history.save(action_doc)
    
    
## Evaluation measures
def measure_performance(gold_handle, verbose=0):
    sys_matrix = defaultdict(lambda: defaultdict(int))
    entity_type_counts = {'per': 0, 'org': 0, 'misc': 0}
    et_per_department_counts = defaultdict(lambda: {'per': 0, 'org': 0, 'misc': 0})    
    
    systems = {
        'PM':          (u'PoliticalMashup/name_spotter_xml.py',
                        u'PoliticalMashup/id-members.xq'),
        'DBpS':        (u'DBpedia Spotlight 0.7 (conf=0.5)',),
        'F+S':         (u'IlpsSemanticizer',),
        'PM>F+S>DBpS': (u'PoliticalMashup/name_spotter_xml.py',
                        u'PoliticalMashup/id-members.xq',
                        u'IlpsSemanticizer',
                        u'DBpedia Spotlight 0.7 (conf=0.5)'),
        'PM>DBpS':     (u'PoliticalMashup/name_spotter_xml.py',
                        u'PoliticalMashup/id-members.xq',
                        u'DBpedia Spotlight 0.7 (conf=0.5)'),
        'PM>F+S':      (u'PoliticalMashup/name_spotter_xml.py',
                        u'PoliticalMashup/id-members.xq',
                        u'IlpsSemanticizer'),
        'F+S>DBpS':    (u'IlpsSemanticizer',
                        u'DBpedia Spotlight 0.7 (conf=0.5)')
    }
    
    gold_ann_count = DB.history.find({
        'link': {'$exists': 1}, 
        'link.url': {'$nin': ['ignore', 'NIL']},
        'handle': gold_handle
    }).count()
    
    for scene in DB.rendered_sample.find():
        if verbose:
            print "Judging scene {}".format(scene['_id'])
        by_surfaceform = links_from_scene(scene)
        
        for ann_id, links in by_surfaceform.iteritems():
            # TODO: need to loop over gold standard if judges can annotate new surfaceforms
            judgement = DB.history.find_one({'_id': '{}_{}'.format(gold_handle, ann_id)})
            if not judgement:
                print "WARNING: no judgement for {}_{}".format(gold_handle, ann_id)
                continue
            gold_slugs = set()
            # TODO: refactor if NIL is a list item
            if judgement['link']['url'] in ('NIL', ['NIL']):
                gold_slugs.add('NIL')
            elif judgement['link']['url'] == ['ignore']:
                # this annotation is not part of the evaluation
                continue
            else:
                for url in judgement['link']['url']:
                    gold_slugs.add(helpers.slugify(url))            
            
            links_by_sys = {link['entity-annotator']: link['url'] for link in links}
            
            type_votes = {'per': 0, 'org': 0, 'misc': 0}
            for link in links:
                if re.search(r'\b(per|Person)\b', link['entity-types']):
                    type_votes['per'] += 1
                if re.search(r'\b(org|Organisation)\b', link['entity-types']):
                    type_votes['org'] += 1
                    
            entity_type = max(type_votes.items(), key=lambda t: t[1])[0]
            if not type_votes[entity_type]:
                entity_type = 'misc'
                
            entity_type_counts[entity_type] += 1
            et_per_department_counts[scene['department']][entity_type] += 1
            
            for sys, sys_ids in systems.items():
                # select one link from each system in the preferred order
                sys_link = None
                for sys_id in sys_ids:
                    if sys_id in links_by_sys:
                        sys_link = links_by_sys[sys_id]
                        break
                
                if sys_link:
                    # Positive                        
                    if helpers.slugify(sys_link) in gold_slugs:
                        sys_matrix[sys]['TP'] += 1
                        sys_matrix[sys][entity_type + 'TP'] += 1
                    else:
                        sys_matrix[sys]['FP'] += 1
                        sys_matrix[sys][entity_type + 'FP'] += 1
                else:
                    # Negative
                    if gold_slugs == {'NIL'}:
                        sys_matrix[sys]['TN'] += 1
                        sys_matrix[sys][entity_type + 'TN'] += 1
                    else:
                        sys_matrix[sys]['FN'] += 1
                        sys_matrix[sys][entity_type + 'FN'] += 1
    
    sys_perf = defaultdict(dict)
    for sys_name, measurements in sys_matrix.iteritems():
        sys_pr = sys_perf[sys_name]['Precision'] = precision(measurements)
        sys_re = sys_perf[sys_name]['Recall'] = recall(measurements, gold_ann_count)
        sys_perf[sys_name]['F1'] = f1(sys_pr, sys_re)
        
        per_pr = sys_perf[sys_name+' (per)']['Precision'] = precision(measurements, entity_type='per')
        per_re = sys_perf[sys_name+' (per)']['Recall'] = recall(measurements, entity_type_counts['per'], entity_type='per')
        sys_perf[sys_name+' (per)']['F1'] = f1(per_pr, per_re)
        
        org_pr = sys_perf[sys_name+' (org)']['Precision'] = precision(measurements, entity_type='org')
        org_re = sys_perf[sys_name+' (org)']['Recall'] = recall(measurements, entity_type_counts['org'], entity_type='org')
        sys_perf[sys_name+' (org)']['F1'] = f1(org_pr, org_re)
    
    print "\nGold standard annotation count: {}".format(gold_ann_count)
    print "\nEntity type counts:", entity_type_counts
    print "\nEntity type counts per department:"
    for dept, counts in et_per_department_counts.items():
        print "   ", dept, counts
    
    
    print "\nRanked systems by performance:"
    for rank, s_p in enumerate(sorted(sys_perf.items(), reverse=True,
                         key=lambda t: t[1]['F1']), start=1):
        print rank, s_p[0]
        for m, v in s_p[1].items():
            print "    {}: {:.3f}".format(m, v)
        if s_p[0][-1] == ')': # per/org
            print "    # sys ann: {}".format(sys_matrix[s_p[0][:-6]][s_p[0][-4:-1]+'TP'] + sys_matrix[s_p[0][:-6]][s_p[0][-4:-1]+'FP'])
        else:
            print "    # sys ann: {}".format(sys_matrix[s_p[0]]['TP'] + sys_matrix[s_p[0]]['FP'])
    print "\n\n"
    
    with open('../logbook/sys_perf.csv', 'wb') as f:
        writer = csv.writer(f)
        writer.writerow(['System', 'Precision', 'Recall', 'F1'])
        for sys_name, perf in sys_perf.items():
            writer.writerow([sys_name, perf['Precision'], perf['Recall'], perf['F1']])
    
    return sys_perf, sys_matrix
    
def links_from_scene(rendered_scene):
    scene_xml = helpers.BeautifulSoup(rendered_scene['xml'], 'xml')
    
    # group links by paragraph and surface form
    links_by_span = defaultdict(dict)
    for link in scene_xml.find_all('link'):

        if not link.text:
            if link.get('party-ref'):
                link.string = u"http://resolver.politicalmashup.nl/{}".format(link['party-ref'])
            elif link.get('member-ref'):
                if ',' in link['member-ref']:
                    print 'Bad annotation: multiple candidates', link
                    continue
                link.string = u"http://resolver.politicalmashup.nl/{}".format(link['member-ref'])
            else:
                print "Link without entity", link
                continue
        
        span = (int(link['entity-start']), int(link['entity-end']))
        p_id = link['entity-element']
        
        new_surfaceform = True
        for existing_span in links_by_span[p_id].keys():
            if helpers.annotation_encompasses(existing_span, span):
                links_by_span[p_id][existing_span].add(link)
                new_surfaceform = False
            elif helpers.annotation_encompasses(span, existing_span):
                links_by_span[p_id][span] = links_by_span[p_id][existing_span] | {link}
                del links_by_span[p_id][existing_span]
                new_surfaceform = False
            if not new_surfaceform:
                break
            
        if new_surfaceform:
            links_by_span[p_id][span] = {link}
            
    links_by_sf = defaultdict(list)
    for p_id, span_links in links_by_span.iteritems():
        for span, links in span_links.iteritems():
            ann_id = 'sys{}-{}_{}'.format(span[0], span[1], p_id)
            for link in links:
                links_by_sf[ann_id].append(link_tag_to_dict(link))
            
    return links_by_sf
    
def link_tag_to_dict(link_tag):
    link_dict = dict(link_tag.attrs)
    link_dict['url'] = link_tag.text
    return link_dict
    
def recall(meas, gold_ann_count, entity_type=''):
    return meas[entity_type + 'TP'] / float(gold_ann_count)
    
def precision(meas, entity_type=''):
    return meas[entity_type + 'TP'] / float(meas[entity_type + 'TP'] 
                                            + meas[entity_type + 'FP'])
    
def f1(precision, recall):
    return 2 * precision * recall / float(precision + recall)