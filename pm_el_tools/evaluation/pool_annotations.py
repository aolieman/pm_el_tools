# -*- coding: utf-8 -*-
"""
Gather the annotations from different systems
and store them in the proceedings using a unified XML schema.
"""
import sys, re
from bs4 import BeautifulSoup
from pm_el_tools.specialized_linkers import name_linker_xml as tailor
from pm_el_tools import helpers
from pm_el_tools.settings import PROCEEDINGS_IDs
import dbp_spotlight_xml as dbps_xml


def pool_for_proceedings(proceedings_xml, verbose=0):
    # Parse it with BeautifulSoup/lxml
    proc_soup = BeautifulSoup(proceedings_xml, "xml")
    
    # Remove the language model from <meta/>
    if proc_soup.meta.find('clouds'):
        proc_soup.meta.find('clouds').clear()
    
    # Copy existing annotations to the p they apply to
    ref_count = 0
    for link in proc_soup.meta.relation.find_all('link'):
        target_url = helpers.wiki_to_dbp_url(link.text)
        last_match = {}
        for ref in link.find_all('reference'):
            sys.stdout.write("\rMoved {} existing annotations...".format(ref_count))
            sys.stdout.flush()
            p_tag = proc_soup.find('p', {'pm:id': ref['entity-element']})
            p_text = "".join(p_tag.find_all(text=True))
            raw_name_pattern = ur'(\b|^|[^a-zA-Z])({})(\b|$|[^a-zA-Z])'
            name_pattern = re.compile(raw_name_pattern.format(re.escape(ref['entity-name'])), re.UNICODE)
            start_pos = last_match.get((p_tag['pm:id'], ref['entity-name']), 0)
            match = name_pattern.search(p_text, start_pos)
            try:
                last_match[(p_tag['pm:id'], ref['entity-name'])] = match.end(2)
            except AttributeError:
                ref['entity-name'] = ref['entity-name'].replace(' ', '')
                name_pattern = re.compile(raw_name_pattern.format(re.escape(ref['entity-name'])), re.UNICODE)
                start_pos = last_match.get((p_tag['pm:id'], ref['entity-name']), 0)
                match = name_pattern.search(p_text, start_pos)
                try:
                    last_match[(p_tag['pm:id'], ref['entity-name'])] = match.end(2)
                except AttributeError:
                    print repr(p_text)
                    print repr(ref['entity-name'])
                    raise
                
            new_link = helpers.make_link(
                proc_soup,
                target_url,
                ref['entity-name'],
                match.start(2),
                match.end(2),
                ref['entity-element'],
                ref['entity-annotator'],
                entity_types=ref['entity-types'],
                member_ref=link.get('pm:member-ref'),
                party_ref=link.get('pm:party-ref'),
                confidence=ref['entity-confidence']
            )
            p_tag.insert_after(new_link)
            ref_count += 1            
            
        # Remove the existing annotation
        link.extract()
        
    sys.stdout.write("\n")
    
    # Generate tailored annotations for people and parties
    annotated_soup, member_disambiguations = tailor.find_people_names(proc_soup, verbose=verbose)
    if verbose:
        print member_disambiguations
        print "\n# of role_and_name: %i" % len(member_disambiguations)
        print "# of failed disambiguations: %i" % len(filter(
            lambda v: v is None, member_disambiguations.values()))
            
    # Generate DBpedia Spotlight annotations
    annotated_soup = dbps_xml.annotate(annotated_soup, verbose=verbose)
            
    return annotated_soup
            
            
if __name__ == "__main__":
    
    for pid in PROCEEDINGS_IDs:
        proc_xml = helpers.resolve_document(pid)
        annotated_soup = pool_for_proceedings(proc_xml, verbose=1)
        fpath = "../annotated_output/xml/{}.xml".format(pid)
        with open(fpath, 'wb') as f:
            f.write(annotated_soup.prettify('utf8'))
    
    print "\n\n"
    for disamb_soup in tailor.failed_disambiguations:
        print "\n", disamb_soup