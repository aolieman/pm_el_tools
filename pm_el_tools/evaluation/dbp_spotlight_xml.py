# -*- coding: utf-8 -*-
"""
DBpedia Spotlight wrapper for generating XML annotations
"""

import spotlight
from pm_el_tools import helpers
from requests.exceptions import HTTPError
from pm_el_tools.settings import DBP_SPOTLIGHT_URL


def annotate(xml_proc_soup, confidence=0.5, verbose=0):
    for p in xml_proc_soup.find_all('p'):
        p_text = u"".join(p.find_all(text=True))
        if not p_text:
            continue
        endpoint = DBP_SPOTLIGHT_URL + "annotate"
        try:
            resp = spotlight.annotate(endpoint, p_text, confidence)
        except spotlight.SpotlightException as e:
            if verbose > 1:
                print e.message
            continue
        except HTTPError:
            print repr(p_text)
            raise
        
        for ann in resp:
            new_link =  helpers.make_link(
                xml_proc_soup,
                ann['URI'],
                unicode(ann['surfaceForm']),
                ann['offset'],
                ann['offset'] + len(unicode(ann['surfaceForm'])),
                p['pm:id'],
                "DBpedia Spotlight 0.7 (conf={})".format(confidence),
                entity_types=ann['types'],
                confidence=ann['similarityScore']
            )
            p.insert_after(new_link)
            
    return xml_proc_soup