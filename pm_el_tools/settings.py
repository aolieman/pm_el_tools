import pymongo

# PoliticalMashup services
PM_SEARCH_URL           = "http://mashup2.science.uva.nl:8004/exist/apps/search/"
PM_AUTOCOMPLETE_PATH    = 'autocomplete/query.xq'
RESOLVER_URL = "http://resolver.politicalmashup.nl/"
EXPORT_URL = "http://backend.politicalmashup.nl/export.xq"
ID_MEMBERS_URL = "http://backend.politicalmashup.nl/id-members.xq"
ODE_RESOLVER_URL        = "http://ode.politicalmashup.nl/resolver/"

# Generalist EL systems
DBP_SPOTLIGHT_URL       = "http://rijsbergen.hum.uva.nl:8082/rest/"

# XML file collections
MEMBERS_PATH = ""

def get_members_path():
    return MEMBERS_PATH

# Annotator interface server
INTERFACE_HOST  = 'localhost'   # set to '0.0.0.0' for public access
INTERFACE_PORT  = 8083
INTERFACE_DEBUG = True          # set to False when allowing public access

# List of example proceedings IDs (without filetype suffix)
PROCEEDINGS_IDs = [
    "nl.proc.ob.d.h-tk-20112012-98-3",
    "nl.proc.ob.d.h-tk-20112012-39-5",
    "nl.proc.ob.d.h-tk-20112012-39-6",
    "nl.proc.ob.d.h-ek-20112012-10-10",
    "nl.proc.ob.d.h-tk-20112012-3-2"    
]


# Overwrite default settings with any specified local settings
try:
    from settings_local import *
except ImportError:
    print 'No settings_local.py found; using default settings.py'
    
    # Initialize MongoDB client
    # NB this code may not run as-is; place a customized version in settings_local.py
    DB_CLIENT = pymongo.MongoClient('localhost')
    # DB_CLIENT.database_name.authenticate('user', 'password')
    DB = DB_CLIENT.database_name