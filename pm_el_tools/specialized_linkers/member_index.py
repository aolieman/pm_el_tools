# -*- coding: utf-8 -*-
"""
Load member XML into a gov. member index.
The index supports temporal search and filtering by member post/portfolio.
"""

import os, re, json
import nltk
from datetime import datetime
from collections import defaultdict
from bs4 import BeautifulSoup
from dateutil.relativedelta import relativedelta
from pm_el_tools.helpers import load_model, dump_model, wiki_to_dbp_url

class CabinetMembers(object):
    """A queryable interface for cabinet member indexes.
       WARNING: queries should not use dates later than the index creation date!
    """
    
    def __init__(self):
        print "Loading cabinet member indexes..."
        self.prime_ministers = load_model('prime_ministers.pickle')
        self.ministers = load_model('ministers.pickle')
        self.secretaries = load_model('secretaries.pickle')
        self.members_of_parliament = load_model('members_of_parliament.pickle')
        
    def find_member(self, date, name, house_or_gov):
        year_list = (
            self.members_of_parliament[date.year] +
            self.ministers[date.year] +
            self.secretaries[date.year]
        )
        
        candidates = [
            member for member in year_list
            if member['pm_from'] <= date < member['pm_till']
            and re.search(
                # replace query name whitespace by wildcards
                re.sub(r'\\\s', r'.*', re.escape(name)),
                member['full_name'],
                re.IGNORECASE | re.UNICODE
            )
        ]
        if len(candidates) > 1:
            candidates = [
                member for member in candidates
                if house_or_gov == (member.get('pm_house') or 'government')
            ]       
            
        if len(candidates) > 1:
            raw_name_pattern = ur'(\b|^|[^a-zA-Z])({})(\b|$|[^a-zA-Z])'
            name_pattern = re.compile(
                raw_name_pattern.format(re.escape(name)), 
                re.IGNORECASE | re.UNICODE
            )
            candidates = [
                member for member in candidates
                if re.search(name_pattern, member['full_name'])
            ]
            
        return single_candidate(candidates, date)
            
    def find_prime_minister(self, date, function="minister-president"):
        year_list = self.prime_ministers[date.year]
        candidates = [
            member for member in year_list
            if member['pm_from'] <= date <= member['pm_till']
            and member['member_function'] == function
        ]
        return single_candidate(candidates, date)
            
    def find_minister(self, date, post):
        year_list = self.ministers[date.year]
        candidates = [
            member for member in year_list
            if member['pm_from'] <= date <= member['pm_till']
            and (member['member_post'] == post
                 or acronym_match(member['member_post'], post))
        ]
        return single_candidate(candidates, date)
            
    def find_secretary(self, date, post):
        year_list = self.secretaries[date.year]
        candidates = [
            member for member in year_list
            if member['pm_from'] <= date <= member['pm_till']
            and (member['member_post'] == post
                 or acronym_match(member['member_post'], post))
        ]
        return single_candidate(candidates, date)

def acronym_match(s1, s2):
    return [c for c in s1 if c.isupper()] == [c for c in s2 if c.isupper()]
    
def single_candidate(candidates, date=None):
    if len(candidates) == 1:
        return candidates[0].copy()
    elif not candidates:
        return None
    else:
        # TODO: raise and catch where expected / allowed
        print "Exception({}, 'Multiple candidates: {}')".format(date, candidates)
        return candidates[0].copy()

prime_ministers = defaultdict(list)
ministers = defaultdict(list)
secretaries = defaultdict(list)
members_of_parliament = defaultdict(list)

def parse_member(file_path):
    with open(file_path) as f:
        member_xml = f.read()
        
    member_soup = BeautifulSoup(member_xml, 'xml')
    
    # NB there are subtle differences between functions and memberships
    gov_functions = member_soup.find_all('function', {'pm:role': 'government'})   
    gov_memberships = member_soup.find_all('membership', {'pm:body': 'government'})
    
    mp_functions = member_soup.find_all('function', {'pm:role': 'mp'})
    
    member_id = member_soup.member['pm:id']
    full_name = member_soup.member.full.text
    dbpedia_link = member_soup.member.find('link', attrs={'pm:description': "Dbpedia (nl)"})
    if not dbpedia_link:
        wikipedia_link = member_soup.member.find('link', attrs={'pm:linktype': "wikipedia"})
        dbpedia_url = wiki_to_dbp_url(wikipedia_link.text) if wikipedia_link else None
    else:
        dbpedia_url = dbpedia_link.text
    
    # Pattern of government post
    post_p = re.compile(r"""   
        (?P<function>                       # name function group
        [Mm]inister|[Ss]taatssecretaris)    # government functions
        (?:\s+(?:van|voor)\s+               # " van/voor " indicates post
        (?P<post>                           # name post group
        [A-Z&]+                             # leading capital or acronym
        [\w]*                               # zero or more lowercase chars
        ((\s+|(,\s+)|(\s+(en|&)\s+))        # " " or ", " or " en "
        [A-Z][\w-]*                         # one capitalized word
        ((\s+|(,\s+)|(\s+(en|&)\s+))        # " " or ", " or " en "
        [A-Z][\w-]*)?)?))                   # capword (optional groups)
    """, re.UNICODE | re.VERBOSE)
    
    for func in mp_functions:
        if not func.period:
            continue
        
        pm_from, pm_till = parse_function_period(func)
        if "eerste kamer" in func.find('name').text.lower():
            pm_house = "senate"
        elif "tweede kamer" in func.find('name').text.lower():
            pm_house = "commons"
        else:
            pm_house = "united"
        
        m_dict = {
            'member_id': member_id,
            'full_name': full_name,
            'pm_from': pm_from,
            'pm_till': pm_till,
            'pm_house': pm_house,
            'dbpedia_url': dbpedia_url
        }
        
        for year in range(pm_from.year, pm_till.year + 1):
                members_of_parliament[year].append(m_dict)
        
    
    for func in gov_functions:
        # get period from function, but post from membership
        overlapping_membership = []
        if not func.period:
            continue
        
        for mship in gov_memberships:
            # membership and function must overlap
            if mship.period and (
                mship.period['pm:from'] <= func.period['pm:till'] 
                and mship.period['pm:till'] >= func.period['pm:from']
            ):
                overlapping_membership.append(mship)
        
        if not overlapping_membership:
            # this is not a cabinet membership, but a minor gov role
            continue
        elif len(overlapping_membership) == 1:
            corr_mship = overlapping_membership[0]
        else:
            corr_mship = sorted(
                overlapping_membership, 
                key=lambda m: edit_ratio(
                    m.find('name').text,
                    func.find('extra', {'pm:type': 'original-name'}).text
                )
            )[0]
        
        pm_from, pm_till = parse_function_period(func, corr_mship)
        post_str = corr_mship.findChild('name').text
        try:
            print "\n", post_str
        except UnicodeError:
            print "\n", repr(post_str)
        
        m_dict = {
            'member_id': member_id,
            'full_name': full_name,
            'pm_from': pm_from,
            'pm_till': pm_till,
            'dbpedia_url': dbpedia_url
        }        
        
        # add (vice) prime-minister function if it has not been selected
        for m in overlapping_membership[1:]:
            function = m.findChild('name').text.lower()
            if function in {"minister-president", "viceminister-president"}:
                president_m_dict = m_dict.copy()
                president_m_dict['member_function'] = function
                for year in range(pm_from.year, pm_till.year + 1):
                    prime_ministers[year].append(president_m_dict)
        
        match = re.search(post_p, post_str)
        if match:
            groupdict = match.groupdict()
            function = groupdict['function'].lower()
            print (groupdict['function'], groupdict['post'])
            
            m_dict['member_function'] = function
            m_dict['member_post'] = groupdict['post']
            
            if function == "minister":
                index = ministers
            elif function == "staatssecretaris":
                index = secretaries
            else:
                raise ValueError("Function %s not recognized" % function)
                
            for year in range(pm_from.year, pm_till.year + 1):
                index[year].append(m_dict)
        else:
            print post_str.lower()
            function = post_str.lower()
            index = None
            
            replacements = {
                "aangelegenheden suriname en de nederlandse antillen": "Surinaamse en Nederlands-Antilliaanse Zaken",
                "aangelegenheden de nederlandse antillen": "Nederlands-Antilliaanse Zaken",
                "aangelegenheden de nederlandse antillen en aruba": "Nederlands-Antilliaanse en Arubaanse Zaken",
                "zonder portefeuille": function.partition('(')[-1].rpartition(')')[0]
            }
            
            if function in {"minister-president", "viceminister-president"}:
                index = prime_ministers
                m_dict.pop('member_post', None)
                
            for rep in replacements:
                if rep in function:
                    index = ministers
                    function = "minister"
                    m_dict['member_post'] = replacements[rep]
                    break
                
            if index is None:
                print "WARN Function %s not recognized" % function
            else:                
                m_dict['member_function'] = function
                for year in range(pm_from.year, pm_till.year + 1):
                    index[year].append(m_dict)   
                    
def parse_function_period(function, corr_membership=None):
    pm_from = datetime.strptime(function.period['pm:from'], "%Y-%m-%d").date()
    try:
        today = datetime.now().date()
        pm_till = datetime.strptime(function.period['pm:till'], "%Y-%m-%d").date()
    except ValueError:
        if function.period['pm:till'] == "present":
            if (corr_membership 
                and relativedelta(today, pm_from).years > 8):
                try:
                    pm_till = datetime.strptime(corr_membership.period['pm:till'], "%Y-%m-%d").date()
                except ValueError:
                    if corr_membership.period['pm:till'] == "present":
                        pm_till = today
                    else:
                        raise
            else:
                pm_till = today
        else:
            raise

    return pm_from, pm_till                    

       
def build_index(members_dir_path):
    for fp in os.listdir(members_dir_path):
        print fp
        parse_member(os.path.join(members_dir_path, fp))
    
    dump_model('prime_ministers.pickle', prime_ministers)
    dump_model('secretaries.pickle', secretaries)
    dump_model('ministers.pickle', ministers)
    dump_model('members_of_parliament.pickle', members_of_parliament)

def members_per_post(start_y=2000, end_y=2014):
    cm = CabinetMembers()
    per_post = defaultdict(set)
    for role in ('ministers', 'secretaries'):
        print "Sorting {}...".format(role)
        for y in xrange(start_y, end_y):
            cab_members = getattr(cm, role)[y]
            for m in cab_members:
                m['pm_from'] = str(m['pm_from'])
                m['pm_till'] = str(m['pm_till'])
                try:
                    per_post[m['member_post']].add(
                        (m['member_id'], m['member_post'], m['pm_from'], m['pm_till'])
                    )
                except TypeError:
                    print "Unhashable:", m
                    continue
            
    for post, member_set in per_post.iteritems():
        per_post[post] = sorted(list(member_set), key=lambda m: m[0])
        
    with open('../models/intermediary/cab_members_per_post.json', 'wb') as f:
        json.dump(per_post, f, indent=2, sort_keys=True)
        
    return per_post
    
def members_per_department():
    # valid for 2000-2015
    # TODO: fix inadvertent truncation of posts
    per_department = members_per_post().copy()
    per_department[u'Zonder ministerie'] = []
    merge_plan = {
            u'Zonder ministerie': (u'Ontwikkelingssamenwerking',
                                   u'Wonen, Wijken en Integratie',
                                   u'Jeugd en Gezin',
                                   u'Bestuurlijke',
                                   u'Vreemdelingenzaken en Integratie',
                                   u'Immigratie en Asiel',
                                   u'Grotesteden',
                                   u'Integratie, Preventie, Jeugdbescherming',
                                   u'Buitenlandse Handel en Ontwikkelingssamenwerking',
                                   u'Wonen en Rijksdienst',
                                   u'Immigratie, Integratie en Asiel'),
            u'Economische Zaken': (u'Economische Zaken, Landbouw',
                                   u'Landbouw, Natuur en Voedselkwaliteit',
                                   u'Landbouw, Natuurbeheer en Visserij'),
            u'Infrastructuur en Milieu': (u'Volkshuisvesting, Ruimtelijke Ordening',
                                          u'Verkeer en Waterstaat'),
            u'Veiligheid en Justitie': (u'Justitie',)
    }
    for department, posts in merge_plan.iteritems():
        for post in posts:
            per_department[department] += per_department[post]
            del per_department[post]
            
    with open('../models/intermediary/cab_members_per_department.json', 'wb') as f:
        json.dump(per_department, f, indent=2, sort_keys=True)
            
    return per_department

def edit_ratio(str1, str2):
    """
    Computes the Edit Ratio [0-1] metric between two strings.
    Indicates the fraction of characters in the longest string
    that need to be changed for str1 to be equal to str2.
    """
    distance = nltk.metrics.edit_distance(str1, str2)
    return float(distance) / max(len(str1), len(str2))
                
if __name__ == "__main__":
    pass
            