# -*- coding: utf-8 -*-
"""
Build a party name tree from XML and map names to identifiers.

"""

import re, pickle
from itertools import groupby
from operator import itemgetter
import requests
from bs4 import BeautifulSoup
from acora import AcoraBuilder
from pm_el_tools.helpers import load_model
from pm_el_tools.settings import RESOLVER_URL, EXPORT_URL


class PartyNameSearcher(object):
    """A queryable interface for party names that are mapped to
       PoliticalMashup identifiers.
    """
    
    def __init__(self):      
        print "Loading party name->(pid, dbp_url) map..."
        self.party_map = load_model('party_map.pickle')
            
        print "Building a name tree from the party map..."
        builder = AcoraBuilder(*[n for n in self.party_map])
        self.name_tree = builder.build()
            
    def find_all(self, s):
        last_match_end = 0
        acora_gen = self.name_tree.finditer(s.lower())
        for kw, pos in self.longest_match(acora_gen):
            name_span = (pos, pos + len(kw))
            context_span = ((name_span[0] or 1) - 1, name_span[1] + 1)
            name_candidate = s[slice(*name_span)]
            exp_candidate = s[slice(*context_span)]
            name_boundary = re.compile(ur"\b{}\b".format(name_candidate), 
                                       re.UNICODE)
            re_match = re.search(name_boundary, exp_candidate)
            preceding_string = s[last_match_end:name_span[0]]
            remaining_string = s[name_span[1]:]
            
            if re_match:
                party_mention = {
                    'name': name_candidate,
                    'pid': self.party_map[name_candidate.lower()][0],
                    'dbp_url': self.party_map[name_candidate.lower()][1],
                    'span': name_span
                }
                last_match_end = name_span[1]
                yield preceding_string, party_mention, remaining_string
            
        if last_match_end == 0:
            yield s, None, None
        elif last_match_end < len(s):
            yield s[last_match_end:], None, None
        
    def longest_match(self, matches):
        for pos, match_set in groupby(matches, itemgetter(1)):
            yield max(match_set)
        

def build_tree_and_map(party_ids):
    # Map party names to IDs in a dictionary
    party_map = {}
    for pid in party_ids:
        party_names, dbp_url = parse_party_xml(pid)
        for name in party_names:
            party_map[name] = (pid, dbp_url)
    party_map.pop("")
    
    # Build an Aho-Corasick tree from the party names
    builder = AcoraBuilder(*[n for n in party_map])
    name_tree = builder.build()
    
    with open("../models/party_map.pickle", 'wb') as f:    
        pickle.dump(party_map, f)
            
    return name_tree, party_map

def parse_party_xml(party_id):
    party_url = RESOLVER_URL + party_id
    party_xml = requests.get(party_url).text
    party_soup = BeautifulSoup(party_xml, "xml")
    name_container = party_soup.party.find("alternative-names")
    dbpedia_link = party_soup.party.find('link',
                                         attrs={'pm:description': "Dbpedia (nl)"})
    dbp_url = dbpedia_link.text if dbpedia_link else None
    if name_container:
        name_elements = name_container.find_all("name")
        return {ne.text.lower() for ne in name_elements}, dbp_url
    else:
        return {party_soup.party.find('name').text.lower()}, dbp_url

def get_party_ids(collection="p/nl"):
    payload = {'collection': collection, 'infofields': 'id', 'view': 'xml'}
    party_collection_xml = requests.get(EXPORT_URL, params=payload).text
    party_list_soup = BeautifulSoup(party_collection_xml, "xml")
    return [row.item['string'] for row in party_list_soup('row')]
    
if __name__ == "__main__":
    ttxt = u"Ik ben het volledig met de heer Pechtold eens dat eenvoudige en duidelijke wetgeving van groot belang is, maar niet ten koste van de rechtvaardigheid. Hij lijkt kost wat kost een heldere wet te willen, ook als die oneerlijk is voor de mensen die het kwetsbaarst zijn. Daar zit een groot onderscheid tussen D66 en GroenLinks. Wij vinden dat deze wet vooral een grotere rechtvaardigheid moet realiseren en niet alleen minder bureaucratie waarop D66 lijkt uit te zijn. Bovendien is het onzin wat de heer Pechtold zegt. In de aanvullende pensioenen en elders wordt allang arbeidshistorie bijgehouden. Het probleem zit in de snelheid waarmee je het wilt invoeren. Het kan uitstekend. De complexiteit van het kabinet zit in 30 jaar en in al die aanvullende toestanden die wij niet nodig hebben. De heer Pechtold zou ons heel goed kunnen volgen."
    pns = PartyNameSearcher()
    for res in pns.find_all(ttxt):
        print res