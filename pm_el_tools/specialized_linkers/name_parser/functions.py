import local_config
import reparse
from collections import defaultdict

def make_mock_function(name):
    def mock_function(*args, **kwargs):
        print name, args, kwargs
        return kwargs or args
        
    return mock_function

functions = {}
for n in [
    'Singular Address',
    'Pleural Address',
    'Singular Function Address',
    'Stand-alone Surname',
    'Capitalized Word',
    'Lowercase Word',
    'Conjunction Element',
    'Center Names',
    'Government Post'
]:
    functions[n] = make_mock_function(n)
    
path = u'G:\\MyData\\Dropbox\\ExPoSe\\pm_el_tools\\pm_el_tools\\specialized_linkers\\name_parser\\'
    
mparser = reparse.parser(
    parser_type=reparse.alt_parser,
    expressions_yaml_path=path + "expressions.yaml",
    patterns_yaml_path=path + "patterns.yaml",
    functions=functions
)

