# The settings in this file are monkeypatched onto the `reparse` namespace
import regex
from reparse import config

regex_flags = regex.VERBOSE | regex.UNICODE
config.regex_flags = regex_flags
