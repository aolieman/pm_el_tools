import local_config
from reparse.tools.expression_checker import check_expression
import unittest
import os
import yaml

class test_patterns(unittest.TestCase):    
    
    def test_name_expressions(self):
        fpath = os.path.join(os.path.dirname(__file__), 'expressions.yaml')
        with open(fpath, 'r') as f:
            expressions_dict = yaml.safe_load(f)
        check_expression(self, expressions_dict)
