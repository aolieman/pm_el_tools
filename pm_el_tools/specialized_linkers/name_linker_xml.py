# -*- coding: utf-8 -*-
"""
Exploration of lightweight spotting and disambiguation
of people and organizations.
"""
import os
import re
import requests
from bs4 import BeautifulSoup
from datetime import datetime
from member_index import CabinetMembers
from party_tree_map import PartyNameSearcher
from pm_el_tools import helpers
from pm_el_tools.settings import get_members_path, RESOLVER_URL


# Initialize cabinet members index
cm = CabinetMembers()

# Initialize the party name tree
pns = PartyNameSearcher()

# Container for failed disambiguations
failed_disambiguations = []


def find_people_names(proc_soup, verbose=0):
    # Get proceedings' date and house
    proc_date = re.sub(r'\s+', '', proc_soup.meta.date.string)
    proc_house = proc_soup.meta.find('house')['pm:house']

    # Keep a cache of found disambiguations
    member_disambiguations = {}
    
    # Local members XML
    members_path = get_members_path()

    # Resolve speaker list, put in cache
    speakers = proc_soup.meta.relation.find_all('person')
    for speaker in speakers:
        member_id = speaker['pm:member-ref']
        if not member_id:
            continue
        member_role = speaker['pm:role']
        member_function = speaker['pm:function'].lower()
        if member_function.startswith("de "):
            member_function = member_function[3:]
            
        try:
            member_path = os.path.join(members_path, '{}.xml'.format(member_id))
            with open(member_path) as f:
                member_xml = f.read()
        except IOError:
            print "Member XML {} not found, falling back to resolver".format(member_path)
            member_url = "%s%s" % (RESOLVER_URL, member_id)
            member_xml = requests.get(member_url).text
        
        
        member_soup = BeautifulSoup(member_xml, "xml")
        last_name = member_soup.member.find('last').string
        full_name = member_soup.member.find('full').string
        role_and_name = (member_role, upcase_lname(last_name))
        dbpedia_link = member_soup.member.find('link',
                                               attrs={'pm:description': "Dbpedia (nl)"})
        
        member_disambiguations[role_and_name] = {
            'member_id': member_id,
            'full_name': full_name,
            'member_function': member_function,
            'last_mentioned': 0,
            'dbpedia_url': dbpedia_link.text if dbpedia_link else None
        }
    
    # Pattern of (last) name(s), found with particular addresses
    people_p = re.compile(r"""   
        # Branch of single mentioned person
        (?P<singular_address>               # name singular_address group
        heer|[Mm]evrouw|lid|[Cc]ollega      # address styles parliament
        |[Mm]inister|[Ss]taatssecretaris)   # address styles government
        \s+                                 # one or more whitespaces
        (?P<single_name>                    # name single_name group
        [A-Z][\w-]+                         # one capitalized word
        (?:(?:\s+[\w']*)?                   # zero or one lowercase word
        (?:\s+[A-Z][\w-]*)+)?)              # zero or more capitalized words
        |
        # Branch of multiple mentioned persons
        (?P<pleural_address>                # name pleural_address group
        leden|heren|collega's|dames         # pleural address styles
        |collegae)
        \s+                                 # one or more whitespaces
        (?P<first_name>                     # name first_name group
        [A-Z][\w-]+                         # one capitalized word
        (?:(?:\s+[\w']*)?                   # zero or one lowercase word
        (?:\s+[A-Z][\w-]*)+)?)              # zero or more capitalized words
        (?P<center_names>                   # name center_names group
        (,\s+[A-Z][\w-]+                    # (as before, but prefixed with
        (?:(?:\s+[\w']*)?                   #  a comma and whitespace;
        (?:\s+[A-Z][\w-]*)+)?)*)            #  zero or more repetitions)
        \s+en\s+                            # " en " indicates final name
        (?P<final_name>                     # name final_name group
        [A-Z][\w-]+                         # one capitalized word
        (?:(?:\s+[\w']*)?                   # zero or one lowercase word
        (?:\s+[A-Z][\w-]*)+)?)              # zero or more capitalized words
        |
        # Branch of person mentioned by function
        \b                                  # word boundary
        (?P<nameless_address>               # name nameless_address group
        (?P<function>                       # name function group
        (?:[Vv]ice)*[Mm]inister(?:-president)*|
        [Ss]taatssecretaris|[Pp]remier|
        bewind\w+|                          # government functions
        [Vv]oorzitter)                      # chairperson
        \W+(?:(?:van|voor)\s+               # " van/voor " indicates post
        (?P<post>                           # name post group
        [A-Z&]+                             # leading capital or acronym
        [\w]*                               # zero or more lowercase chars
        ((\s+|(,\s+)|(\s+(en|&)\s+))        # " " or ", " or " en "
        [A-Z][\w-]*                         # one capitalized word
        ((\s+|(,\s+)|(\s+(en|&)\s+))        # " " or ", " or " en "
        [A-Z][\w-]*)?)?                     # capword (optional groups)
        |[\w-]+))?)                         # or one lowercase word
    """, re.UNICODE | re.VERBOSE)
    
    # Populate the members disambiguations cache
    for p in proc_soup.find_all('p'):
        p_text = u"".join(p.find_all(text=True))
        if not p_text:
            continue
        res_iter = re.finditer(people_p, p_text)
        for c, res in enumerate(res_iter):
            resolve_match(res, proc_date, proc_house, member_disambiguations,
                          p_tag=p, verbose=0)
                
    # Annotate mentioned members and parties
    for p in proc_soup.find_all('p'):
        p_text = u"".join(p.find_all(text=True))
        if not p_text:
            continue
         # Annotate mentioned members
        res_iter = re.finditer(people_p, p_text)
        for c, res in enumerate(res_iter):
            ann_list = resolve_match(res, proc_date, proc_house, member_disambiguations,
                                     p_tag=p, verbose=verbose)
            for ann in ann_list:
                member_link =  helpers.make_link(
                    proc_soup,
                    ann['dbpedia_url'],
                    ann['surfaceform'],
                    ann['start'],
                    ann['end'],
                    ann['last_mentioned'],
                    "PoliticalMashup/name_spotter_xml.py",
                    entity_types="per",
                    member_ref=ann['member_id']
                )
                p.insert_after(member_link)
    
        # Annotate party name mentions
        for preceding_str, party_mention, _sq in pns.find_all(p_text):
            try:
                if not should_annotate_party(preceding_str, party_mention, p):
                    continue
                if verbose:
                    print party_mention
                party_link = helpers.make_link(
                    proc_soup,
                    party_mention['dbp_url'],
                    party_mention['name'],
                    party_mention['span'][0],
                    party_mention['span'][1],
                    p['pm:id'],
                    "PoliticalMashup/name_spotter_xml.py",
                    entity_types="org",
                    party_ref=party_mention['pid']
                )
                p.insert_after(party_link)
            except TypeError:
                pass
        
    return proc_soup, member_disambiguations
        
def should_annotate_party(preceding_str, party_mention, p_tag):
    only_if_cap = {
        "nieuw nederland", "lef", "volkspartij", "eb", "mens",
        "vrije boeren", "ab", "jong", "ton"
    }
    # TODO filter ambiguous acronyms based on session/seats
    highly_ambiguous = {
        "wo", "cd", "c.d.", "bp", "var", "lsp"
    }    
    
    if p_tag.parent.find('link', {
        'pmx:entity-start': party_mention['span'][0],
        'pmx:entity-annotator': "PoliticalMashup/name_spotter_xml.py"
    }):
        # this name has already been annotated as a member
        return False
    elif preceding_str.endswith('-') or preceding_str.endswith('/'):
        # This name is part of a longer name
        return False
    elif (party_mention['name'].lower() in only_if_cap and 
          party_mention['name'].islower()):
        return False
    elif party_mention['name'].lower() in highly_ambiguous:
        return False
    else:
        return True

def disambiguate_last_name(last_name, role, member_function,
                           date, house_or_gov, disambiguations_cache=None):
                              
    if disambiguations_cache is None:
        disambiguations_cache = {}
    
    member = cm.find_member(date, last_name, house_or_gov)    
    
    if not member:
        # Check cache for `role` == "government"
        gov_res = disambiguations_cache.get(("government", last_name))
        if gov_res:
            return gov_res
        # Check cache for a longer/shorter version of this name
        for key in disambiguations_cache:
            if key[0] == role and (key[1].startswith(last_name)
				                or last_name.startswith(key[1])):
                return disambiguations_cache[key]
                
        return member
    else:
        member_dict = member.copy()
        member_dict['last_mentioned'] = 0
        member_dict['member_function'] = member_function
        
        return member_dict


def upcase_lname(s):
    return s[0].upper() + s[1:]


def resolve_match(match, proc_date=None, proc_house=None, member_disambiguations=None,
                  p_tag=None, verbose=0):
    # Parse match object
    groupdict = match.groupdict()
    last_names = []
    if groupdict.get('singular_address'):
        address_style = groupdict['singular_address']
        last_names = [groupdict['single_name']]
    elif groupdict.get('pleural_address'):
        address_style = groupdict['pleural_address']
        last_names = [groupdict['first_name']]
        if groupdict['center_names']:
            last_names += re.split(r',\s+', groupdict['center_names'])[1:]
        last_names.append(groupdict['final_name'])
    elif groupdict.get('nameless_address'):
        address_style = groupdict['function']
        post = groupdict['post']
    else:
        raise Exception("No address found: check regex definition!")
        
    disambiguation_list = []
    annotation_list = []
    function = address_style.lower()
    date = datetime.strptime(proc_date, "%Y-%m-%d").date()
    disamb_failed = {
            'member_id': "#",
            'full_name': "Deze naam of functie is niet correct herkend.",
            'member_function': None,
            'last_mentioned': 0,
            'dbpedia_url': None
    }
        
    if groupdict.get('nameless_address'):
        disamb = None
        # Is the post specified?
        if post:
            # lookup by function, date, post
            if function == "minister":
                disamb = cm.find_minister(date, post)
            elif function == "staatssecretaris":
                disamb = cm.find_secretary(date, post)
                
        elif function in {
            "minister-president", "viceminister-president", "premier"
        }:
            # TODO: search corpus for "vicepremier"            
            if function == "premier":
                disamb = cm.find_prime_minister(date, "minister-president")
            else:
                disamb = cm.find_prime_minister(date, function)
            
        if disamb is None and (not post or post.islower()):
            # Is there a single speaker with this function?
            memb_with_function = [
                m for m in member_disambiguations.values()
                if m and m['member_function'] == function
            ]
            if function.startswith("bewind"):
                memb_with_function = [
                    member_disambiguations[k] for k in member_disambiguations
                    if k[0] == u"government" and member_disambiguations[k]
                ]
            
            if len(memb_with_function) == 1:
                disamb = memb_with_function[0]
                disamb['last_mentioned'] = p_tag['pm:id']
            elif memb_with_function:
                # Select the last-mentioned minister/secretary
                # NB this is an inperfect sort: u'.1.4.2' > u'.1.4.10'
                disamb = sorted(memb_with_function, reverse=True,
                                key=lambda m: m['last_mentioned'])[0]
                disamb['last_mentioned'] = p_tag['pm:id']
            else:
                disamb = None
                
            if post and post.islower() and disamb:
                # Assume the found post is not actually a post
                disamb['start'] = match.start()
                disamb['end'] = match.start() + len(function)
                disamb['surfaceform'] = function
                
        disambiguation_list.append(disamb)
        # TODO: check corpus for mentions of foreign cabinet members
        if disamb is None:
            disamb = disamb_failed.copy()
            member_disambiguations[groupdict['nameless_address']] = None
            failed_disambiguations.append(groupdict['nameless_address'])
        elif not disamb.get('start'):
            disamb['start'] = match.start()
            disamb['end'] = match.end()
            disamb['surfaceform'] = groupdict.get('nameless_address')
            disamb['last_mentioned'] = p_tag['pm:id']
            
        if not post:
            disamb['start'] = match.start()
            disamb['end'] = match.start() + len(function)
            disamb['surfaceform'] = function
                
    else:    
        # Guess member role(s) based on address style
        if function in {"minister", "staatssecretaris", "premier"}:
            member_role = "government"
            house_or_gov = "government"
        else:
            member_role = "mp"
            house_or_gov = proc_house
        
        for last_name in last_names:
            # Get disambiguation from cache or server
            role_and_name = (member_role, last_name)
            try:
                disamb = member_disambiguations[role_and_name]
                if disamb:
                    disamb['last_mentioned'] = p_tag['pm:id']
                    disamb['surfaceform'] = last_name
                elif verbose:
                    print groupdict
            except KeyError:
                disamb = disambiguate_last_name(
                    last_name, member_role, function,
                    date, house_or_gov, member_disambiguations
                )
                if disamb:
                    disamb['last_mentioned'] = p_tag['pm:id']
                    disamb['surfaceform'] = last_name
                elif verbose:
                    print groupdict
                member_disambiguations[role_and_name] = disamb
                
            # Append the found disambiguation to the disamb. list
            disambiguation_list.append(disamb)
            
        
    # Add start and end positions to the disambiguations
    if groupdict.get('pleural_address'):
        start_pos = match.start() + len(address_style) + 1
        for i, disamb in enumerate(disambiguation_list[:-1]):
            end_pos = start_pos + len(last_names[i])
            if disamb:
                disamb['start'] = start_pos
                disamb['end'] = end_pos
                annotation_list.append(disamb)
            start_pos = end_pos + 2 # len(", ")
        start_pos += 2 # len(" en ") - len(", ")
        if disambiguation_list[-1]:
            disambiguation_list[-1]['start'] = start_pos
            disambiguation_list[-1]['end'] = start_pos + len(last_names[-1])
            annotation_list.append(disambiguation_list[-1])
    elif groupdict.get('nameless_address'):
        # positions should have been set already
        if disamb and disamb['member_id'] != '#':
            annotation_list.append(disamb)
    else:
        start_pos = match.start() + len(address_style) + 1
        if disamb:
            disambiguation_list[0]['start'] = start_pos
            disambiguation_list[0]['end'] = start_pos + len(last_names[0])
            annotation_list.append(disamb)
    
    if verbose:
        # Print the found regex groups and disambiguations
        print match.span(), address_style, (last_names or post or None)
        print "    ", disambiguation_list
        
    return annotation_list
    

if __name__ == "__main__":

    pass
