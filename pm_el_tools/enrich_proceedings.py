import argparse
import os
import sys
import stopit
import multiprocessing as mpr
import logging
from requests.exceptions import HTTPError, ConnectionError
from pm_el_tools.evaluation.pool_annotations import pool_for_proceedings
from pm_el_tools.helpers import get_sorted_file_list
from pm_el_tools import settings


# logger initialization
logger = mpr.log_to_stderr()
logger.setLevel(logging.INFO)


def parse_cli_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir", help="Path to the directory that contains unprocessed proceedings XML")
    parser.add_argument("output_dir", help="Directory to write processed XML to (will create a sub-directory)")
    parser.add_argument("-t", "--timeout", type=int, default=120, help="The max. # of seconds that one proceedings file can be processed")
    parser.add_argument("-w", "--workers", type=int, default=max(1, mpr.cpu_count() / 2), choices=range(1, 17), 
                        help="The number of worker processes to run in parallel")
    return parser.parse_args()
    

    
def pool_annotations_for_file(input_dir, output_dir, fname, timeout=120):
    input_fpath = os.path.join(input_dir, fname)
    input_rp, output_subdir = os.path.split(input_dir)
    if not output_subdir:
        output_subdir = os.path.basename(input_rp)
    output_dirpath = os.path.join(output_dir, output_subdir)
    if not os.path.exists(output_dirpath):
        os.makedirs(output_dirpath)
    output_fpath = os.path.join(output_dirpath, fname)
    # return early if the output file exists
    try:
        if os.stat(output_fpath).st_size > 0:
            logger.info("{} already exists in {}".format(fname, output_dirpath))
            return
    except OSError:
        pass
        
    with open(input_fpath, 'rb') as f:
        proc_xml = f.read()
        
    infile_size_in_kb = int(os.stat(input_fpath).st_size / 1024)
        
    # annotate the proceedings with a timeout of max(<file size in kb>, <global timeout>)
    file_timeout = max(timeout, infile_size_in_kb)
    with stopit.ThreadingTimeout(file_timeout) as to_mgr:
        try:
            ann_soup = pool_for_proceedings(proc_xml)
        except AttributeError as e:
            logger.error('ERROR regex failed to move annotation {}'.format(e), exc_info=True)
            return
        except HTTPError as e:
            logger.error('ERROR malformed DBp Spotlight request {}'.format(e), exc_info=True)
            return
        except ConnectionError as e:
            logger.error('ERROR DBpS connection issue {}'.format(e), exc_info=True)
            return
    if to_mgr:
        with open(output_fpath, 'wb') as f:
            f.write(ann_soup.prettify('utf8'))
    else:
        logger.error("ERROR: annotating {} timed out after {} seconds".format(fname, file_timeout), exc_info=True)
            
    
def pool_annotations_parallel(args_tuple):
    input_dir, output_dir, fname, timeout = args_tuple
    logger.info("Pooling annotations for {}".format(fname))
    pool_annotations_for_file(input_dir, output_dir, fname, timeout)
    return "Worker is done with {}".format(fname)
    

def try_members_path(output_dir):
    # Try to specify the members xml path
    if not settings.MEMBERS_PATH:
        output_dir_parent = os.path.join(output_dir, '..')
        expected_path = os.path.abspath(os.path.join(output_dir_parent, 'members'))
        settings.MEMBERS_PATH = expected_path
        logger.info("Looking for Members XML in: {}".format(expected_path))

if __name__ == "__main__":
    args = parse_cli_arguments()
        
    try_members_path(args.output_dir)
        
    if args.workers > 1:
        logger.info("Starting process pool with {} workers".format(args.workers))
        pool = mpr.Pool(
            processes=args.workers,
            initializer=try_members_path,
            initargs=(args.output_dir,)
        )
        args_tuples_list = [
            (args.input_dir, args.output_dir, fname, args.timeout)
            for fname in get_sorted_file_list(args.input_dir)
        ]
        result = pool.map_async(
            pool_annotations_parallel,
            args_tuples_list,
            chunksize=5,
            callback=logger.info
        )
        result.get()
        pool.close()
        pool.join()
    else:
        for fname in get_sorted_file_list(args.input_dir):
            logger.info("Pooling annotations for {}".format(fname))
            pool_annotations_for_file(args.input_dir, args.output_dir, fname, args.timeout)
