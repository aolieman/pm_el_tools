import urllib2
import requests
import wikipedia
import pickle
import os
import re
from bs4 import BeautifulSoup
from pm_el_tools.settings import ODE_RESOLVER_URL


def make_link(soup, url, surfaceform, start, end, element_ref, annotator,
              linktype="dbpedia", entity_types="misc", member_ref=None, 
              party_ref=None, confidence=1):
                  
    new_link = soup.new_tag('link', namespace="http://www.politicalmashup.nl", nsprefix='pm')
    
    if url and slug_from_dbp_url(url).strip():
        new_link.string = url
        
    new_link['pmx:entity-name'] = surfaceform
    new_link['pmx:entity-start'] = start
    new_link['pmx:entity-end'] = end
    new_link['pmx:entity-element'] = element_ref
    new_link['pmx:entity-annotator'] = annotator
    new_link['pm:linktype'] = linktype
    new_link['pmx:entity-types'] = entity_types
    new_link['pmx:entity-confidence'] = confidence
    
    if member_ref:
        new_link['pm:member-ref'] = member_ref
    if party_ref:
        new_link['pm:party-ref'] = party_ref
        
    return new_link
    

def wiki_to_dbp_url(wiki_url):
    wiki_slug = wiki_url.split('wiki/')[-1]
    # dbp_slug = urllib2.unquote(wiki_slug).decode('utf8')
    return u'http://nl.dbpedia.org/resource/{}'.format(wiki_slug)
    
def slug_from_dbp_url(dbp_url):
    dbp_slug = dbp_url.split('resource/')[-1]
    return urllib2.unquote(dbp_slug.encode('utf8')).decode('utf8')
    

def resolve_document(politicalmashup_id, parameters=None):
    # Retrieve a proceedings document
    resp = requests.get(ODE_RESOLVER_URL + politicalmashup_id, params=parameters or {})
    return resp.text
    
def scenes_for_topic(topic_id):
    # return the set of scenes from the given topic
    proc_xml = resolve_document(topic_id)
    proc_soup = BeautifulSoup(proc_xml, "xml")
    scenes = proc_soup.find_all('scene')
    return list(scenes)
    
def get_topic_id(politicalmashup_id):
    pmid_split = politicalmashup_id.split('-')
    return '-'.join(pmid_split[:-1] + pmid_split[-1].split('.')[:1])
    
def get_scene_id(p_id):
    p_id_split = p_id.split('-')
    return '-'.join(p_id_split[:-1] + ['.'.join(p_id_split[-1].split('.')[:3])])
    
def get_scene_html(scene_id):
    topic_id = get_topic_id(scene_id)
    topic_html = resolve_document(topic_id, parameters={'view': 'html'})
    topic_soup = BeautifulSoup(topic_html, 'html5lib')
    return topic_soup.find(id=scene_id), topic_soup

# set wikipedia client language to Dutch
wikipedia.set_lang('nl')
def wikipedia_search(surface_form, limit=3):
    for title in wikipedia.search(surface_form, results=limit):
        try:
            page = wikipedia.WikipediaPage(title)
            yield page.url, title
        except wikipedia.DisambiguationError:
            continue
        
def force_quote(uni_str):
    return urllib2.quote(uni_str.encode('utf8'), safe='/:').decode('utf8')
    
def force_unquote(uni_str):
    return urllib2.unquote(uni_str.encode('utf8')).decode('utf8')
    
def annotation_encompasses(span1, span2):
    return span1[0] <= span2[0] and span1[1] >= span2[1]
    
def slugify(url):
    uurl = force_unquote(url)
    if 'wiki/' in uurl:
        return uurl.split('wiki/')[-1]
    elif 'resource/' in uurl:
        return uurl.split('resource/')[-1]
    elif 'resolver' in uurl:
        return uurl.split('.nl/')[-1]
    else:
        print "Malformed URL: {}".format(uurl)
        
def load_model(model_fname):
    fpath = os.path.join(os.path.dirname(__file__), 'models', model_fname)
    with open(fpath, 'rb') as f:
        loaded_model = pickle.load(f)
    return loaded_model
    
def dump_model(model_fname, model_obj):
    fpath = os.path.join(os.path.dirname(__file__), 'models', model_fname)
    with open(fpath, 'wb') as f:
        pickle.dump(model_obj, f)
        
def extract_chronological_part(pm_identifier):
    try:
        id_parts = pm_identifier.split('-')[2:]
        chrono_parts = ['{:0>4d}'.format(int(re.sub(r'\D', '', pt))) for pt in id_parts]
        return ''.join(chrono_parts)
    except ValueError:
        return ''.join(pm_identifier.split('-')[1:])
    
def get_sorted_file_list(input_dir, key=extract_chronological_part, reverse=True):
    files = [f for f in os.listdir(input_dir) if os.path.isfile(os.path.join(input_dir, f))]
    return sorted(files, key=key, reverse=reverse)
        