import argparse
import os
import sys
import multiprocessing as mpr
import logging
import re
import csv
import json
import signal

from bs4 import BeautifulSoup, element
from collections import defaultdict
from pm_el_tools.helpers import get_sorted_file_list


# logger initialization
logger = mpr.log_to_stderr()
logger.setLevel(logging.INFO)


def parse_cli_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_dir", help="Path to the directory that contains annotated proceedings XML")
    parser.add_argument("out_name", help="Base name for exported annotation files")
    parser.add_argument("-o", "--output_dir", help="Directory to write exported annotations to (will create a sub-directory)")
    parser.add_argument("-w", "--workers", type=int, default=max(1, mpr.cpu_count() / 2), choices=range(1, 17), 
                        help="The number of worker processes to run in parallel")
    parser.add_argument("-pe", "--per_element", action="store_true", default=False,
                        help="Write to a separate file for each PM identifier")
    return parser.parse_args()


def get_filenames_by_year(input_dir):
    year_pattern = re.compile(r'\b\d{8}\b')
    year_map = defaultdict(list)
    
    for fname in get_sorted_file_list(input_dir):
        m = re.search(year_pattern, fname)
        if m:
            year_map[m.group()].append(fname)
        else:
            year_map['misc'].append(fname)
            
    return year_map
    
    
def parse_annotations_from_file(input_dir, fname):
    with open(os.path.join(input_dir, fname), 'r') as f:
        proceedings_xml = f.read()
    
    proc_soup = BeautifulSoup(proceedings_xml, "xml")
    if not proc_soup.proceedings:
        return []
        
    annotations = []
    
    for root_element in proc_soup.proceedings:
        if isinstance(root_element, element.Tag):
            if root_element.name != 'topic':
                print 'Unexpected tag: {} {}'.format(root_element.name, root_element.attrs)
            else:
                annotations += get_links_with_gp_offsets(root_element)
                
    return annotations
    

def norm_str(s):
    out = s.strip()
    return re.sub(r'\s+', ' ', out)
    
# TODO: find & fix issue with broken gov. member annotations
def get_links_with_gp_offsets(root_element):
    annotations = []
    gp_offs = defaultdict(int)
    
    for e in root_element.next_elements:
        seen_root = False
        
        if isinstance(e, element.NavigableString) and not e.parent.name == 'link':
            for p in e.parentGenerator():
                if p.get('pm:id'):
                    if gp_offs[p['pm:id']] == 0:
                        gp_offs[p['pm:id']] += len(norm_str(e))
                    elif len(norm_str(e)):
                        gp_offs[p['pm:id']] += len(norm_str(e)) + 1
                        
                if p == root_element:
                    seen_root = True
                    break
                
            if not seen_root:
                break

        elif e.name == 'link':
            ann_dict = e.attrs.copy()
            ann_dict['grandparent_offsets'] = {}
            dbp_url = e.text.strip()
            if dbp_url:
                ann_dict['dbpedia_url'] = dbp_url
            
            for p in e.parentGenerator():
                if p.get('pm:id'):
                    gp_offset = gp_offs[p['pm:id']] - gp_offs[e['entity-element']]
                    ann_dict['grandparent_offsets'][(p.name, p['pm:id'])] = gp_offset
                
                if p == root_element:
                    seen_root = True
                    break
                
            if seen_root:
                annotations.append(ann_dict)
            else:
                break
            
    return annotations
                

def write_csv_annotations(annotations, output_file):
    fieldnames = [
        'entity-element', 
        'entity-start',
        'entity-end', 
        'entity-name',
        'dbpedia_url', 
        'pm:member-ref',
        'pm:party-ref', 
        'entity-types',
        'entity-annotator',
        'entity-confidence', 
        'pm:linktype',
    ]
    writer = csv.DictWriter(output_file, fieldnames=fieldnames)

    writer.writeheader()
    
    for ann in annotations:
        ann_copy = ann.copy()
        ann_copy.pop('grandparent_offsets', None)
        writer.writerow({k: u'{}'.format(v).encode('utf8') for k, v in ann_copy.items()})
        
# TODO: fix number types, split entity-types, ...        
def write_json_annotations(annotations, output_file):
    out_ann = [stringify_keys(ann) for ann in annotations]
    json.dump(out_ann, output_file, indent=4)


def stringify_keys(ann_dict):
    if ann_dict.get('grandparent_offsets'):
        safe_ann = ann_dict.copy()
        safe_ann['grandparent_offsets'] = {
            '_'.join(k): v for k, v in safe_ann['grandparent_offsets'].items()
        }
        return safe_ann

    return ann_dict

WRITERS = {
    'csv': write_csv_annotations,
    'json': write_json_annotations
}
SUPPORTED_FORMATS = tuple(WRITERS.keys())
    
def export_annotations_for_year(input_dir, output_name, fname_list, 
                                output_dir=None, per_pm_id=False,
                                formats=SUPPORTED_FORMATS):
    if not output_dir:
        output_dir = input_dir
            
    annotations = []
    for fname in fname_list:
        if per_pm_id:
            doc_anns = parse_annotations_from_file(input_dir, fname)
            per_element = defaultdict(list)
            
            for ann in doc_anns:
                grandparent_offsets = ann.pop('grandparent_offsets')
                
                for gp_id, offset in grandparent_offsets.items():
                    updated_elink = ann.copy()
                    updated_elink['entity-start'] = int(updated_elink['entity-start'])
                    updated_elink['entity-start'] += offset
                    updated_elink['entity-end'] = int(updated_elink['entity-end'])
                    updated_elink['entity-end'] += offset
                    per_element[gp_id].append(updated_elink)

            for gp_id, elinks in per_element.items():
                gp_type, oname = gp_id
                write_annotations_to_formats(elinks, output_dir, oname, formats,
                                             subdir=gp_type)
            
        else:
            annotations += parse_annotations_from_file(input_dir, fname)

    if not per_pm_id:
        write_annotations_to_formats(annotations, output_dir, output_name, formats)


def write_annotations_to_formats(annotations, output_dir, output_name, formats, subdir=None):
    for ext in formats:
        export_annotations = WRITERS[ext]
        
        if subdir:
            output_dirpath = os.path.join(output_dir, ext, subdir)
        else:
            output_dirpath = os.path.join(output_dir, ext)
            
        if not os.path.exists(output_dirpath):
            os.makedirs(output_dirpath)
            
        output_fpath = os.path.join(output_dirpath, '.'.join((output_name, ext)))
        with open(output_fpath, 'wb') as out_file:
            export_annotations(annotations, out_file)
    
            
    
def export_annotations_parallel(args_tuple):
    input_dir, output_name, fname_list, out_dir, per_element = args_tuple
    logger.info("Exporting annotations for {}".format(output_name))
    export_annotations_for_year(input_dir, output_name, fname_list,
                                out_dir, per_element)
    return "Worker is done with {}".format(output_name)


if __name__ == "__main__":
    args = parse_cli_arguments()
    
    year_map = get_filenames_by_year(args.input_dir)
    
    if args.workers > 1:
        logger.info("Starting process pool with {} workers".format(args.workers))
        original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        pool = mpr.Pool(processes=args.workers)
        signal.signal(signal.SIGINT, original_sigint_handler)
        
        args_tuples_list = [
            (
                args.input_dir,
                '-'.join((args.out_name, year_str)),
                fname_list,
                args.output_dir,
                args.per_element
            )
            for year_str, fname_list in year_map.items()
        ]
        
        result = pool.map_async(
            export_annotations_parallel,
            args_tuples_list,
            callback=logger.info
        )
        
        # Get the result (timeout needed to catch SIGINT)
        # TODO split the task up and keep track of progress
        result.get(10**6)
        pool.close()
        pool.join()
    else:
        for year_str, fname_list in year_map.items():
            output_name = '-'.join((args.out_name, year_str))
            logger.info("Pooling annotations for {}".format(output_name))
            export_annotations_for_year(args.input_dir, output_name, fname_list,
                                        output_dir=args.output_dir,
                                        per_pm_id=args.per_element)
