# -*- coding: utf-8 -*-
"""
Created on Fri Jan 30 23:15:52 2015

@author: Alex
"""
import requests
from bs4 import BeautifulSoup
from pm_el_tools.settings import PM_SEARCH_URL, PM_AUTOCOMPLETE_PATH

debug_soup = None

def pm_search(q="", speakers="", role="", party="", house="", startdate="", enddate="", *args, **kwargs):
    qstring = {
        'q': q, 'speakers': speakers, 'role': role, 'party': party,
        'party-members': "", 'house': house, 'startdate': startdate,
        'enddate': enddate, 'granularity': 'speech', 'order': 'relevance',
        'view': 'xml'
    }
    qstring.update(kwargs)
    resp = requests.get(PM_SEARCH_URL, params=qstring)
    res_xml = resp.text
    res_soup = BeautifulSoup(res_xml)
    try:
        if res_soup.set['rows'] == '0':
            print 'No results for query', qstring
            return
            yield
        header_items = res_soup.set.find_all('item', recursive=False)
        n_of_columns = int(header_items[0]['colspan'])
        headers = [i['string'] for i in header_items[1:]]
        assert len(headers) == n_of_columns, "did not find the correct headers"
        
        for row in res_soup.find_all('row'):
            items = row.find_all('item', recursive=False)
            yield {headers[k]: items[k]['string'] for k in xrange(n_of_columns)}
            
    except Exception:
        global debug_soup
        debug_soup = res_soup
        print "\n\nAssigned soup to `pm_search_client.debug_soup`"
        raise

def member_autocomplete(q, country='nl'):
    resp = requests.get(PM_SEARCH_URL + PM_AUTOCOMPLETE_PATH, params={'term': q})
    try:
        members = resp.json()
        return [m for m in members if m['id'].startswith(country)]
    except:
        return []