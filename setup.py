from setuptools import setup, find_packages

setup(
    name = "pm_el_tools",
    version = "0.1",
    packages = find_packages(),
    scripts = [
        'pm_el_tools/evaluation/pool_annotations.py',
        'pm_el_tools/evaluation/serve_sample.py',
        'pm_el_tools/enrich_proceedings.py'
    ],
    include_package_data = True,
    install_requires = [
        'requests',
        'pymongo>=2.7.2',
        'acora>=1.8',
        'beautifulsoup4>=4.2.0',
        'Flask>=0.10',
        'pyspotlight',
        'wikipedia',
        'stopit',
        'dateutils>=0.6.6',
        'nltk',
        'lxml'
    ],

    author = "Alex Olieman",
    author_email = "olieman@uva.nl",
    description = "PoliticalMashup Entity Linking tools",
    license = "MIT",
    keywords = "entity linking evaluation",
    url = "http://bitbucket.com/aolieman/pm_el_tools",
)