# PoliticalMashup Entity Linking toolbox

## Installation
Recommended method:

1. clone this repository onto your machine
2. (optional) activate a python `virtualenv`
3. install dependencies and the `pm_el_tools` package:
```bash
$ pip install -r requirements.txt
```

## Command-line usage

Annotating PoliticalMashup XML proceedings
```bash
$ # First download a suitable PoliticalMashup XML archive (to annotate)
$ wget http://data.politicalmashup.nl/permanent/archive/nl/d-nl-proc-ob.tar.gz
$ tar -xzvf d-nl-proc-ob.tar.gz
$ mkdir processed

$ python -m pm_el_tools.enrich_proceedings -h
Loading cabinet member indexes...
Loading party name->(pid, dbp_url) map...
Building a name tree from the party map...
usage: enrich_proceedings.py [-h] [-t TIMEOUT]
                             [-w {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}]
                             input_dir output_dir

positional arguments:
  input_dir             Path to the directory that contains unprocessed
                        proceedings XML
  output_dir            Directory to write processed XML to (will create a
                        sub-directory)

optional arguments:
  -h, --help            show this help message and exit
  -t TIMEOUT, --timeout TIMEOUT
                        The max. # of seconds that one proceedings file can be
                        processed
  -w {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}, --workers {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
                        The number of worker processes to run in parallel

$ python -m pm_el_tools.enrich_proceedings permanent/d/nl/proc/ob processed/ -t=300
$ # Terminal output (stdout) omitted in this example
```
The "processed/ob" directory now contains XML proceedings with embedded links

To export the entity links to CSV and JSON, also run:
```bash
$ python -m pm_el_tools.export_annotations -h
usage: export_annotations.py [-h] [-o OUTPUT_DIR]
                             [-w {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}]
                             input_dir out_name

positional arguments:
  input_dir             Path to the directory that contains annotated
                        proceedings XML
  out_name              Base name for exported annotation files

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        Directory to write exported annotations to (will
                        create a sub-directory)
  -w {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}, --workers {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}
                        The number of worker processes to run in parallel

$ python -m pm_el_tools.export_annotations processed/ob annotations-nl.proc.ob
```
When this script has finished, the "processed/ob" directory contains csv and json subdirectories
